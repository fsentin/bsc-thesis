# Prototype of an Information System for Volunteering Purposes

This thesis aimed to offer a technological solution for engaging society in
volunteering. The offered solution is a multi-platform information system that connects interested volunteers and organizations that need volunteers in a fast, simple, and likable way. The architecture of the system is designed based on the REST architectural style. Implemented parts of the system are the mobile application made in Flutter and the server application made in Spring Boot. 

## Mobile application


## Server application


